import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import { connection } from './imports/database/collections.js'
import verifyUser from './imports/helpers/verifyUser.js'
import resolvers from './imports/graphql/resolvers/resolvers.js'
import typeDefs from './imports/graphql/typeDefs/typeDefs.js'

const port = process.env.PORT

const apolloserver = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const { userId, operatorId } = await verifyUser(req)
    return { userId, operatorId }
  },
})

const app = express()
apolloserver.applyMiddleware({ app })

app.get('/', (req, res) => {
  res.send('hello world')
})

app.listen(port, async () => {
  await connection()
  console.log(`🚀  Server ready at ${port}`)
})

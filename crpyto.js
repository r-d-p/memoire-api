import crypto from 'crypto'
import secretsConfig from './imports/helpers/secretsConfig'

const { CRYPTO_ALGORITHM, CRYPTO_RANDOM_BYTES, CRYPTO_SECRET_KEY } = secretsConfig
const randomBytes = parseInt(CRYPTO_RANDOM_BYTES, 10)
const iv = crypto.randomBytes(randomBytes)
const encrypt = (text) => {
  const cipher = crypto.createCipheriv(CRYPTO_ALGORITHM, CRYPTO_SECRET_KEY, iv)
  const encrypted = Buffer.concat([cipher.update(text), cipher.final()])
  const hash = {
    iv: iv.toString('hex'),
    content: encrypted.toString('hex'),
  }
  return JSON.stringify(hash)
}

const decrypt = (text) => {
  const hash = JSON.parse(text)
  const decipher = crypto.createDecipheriv(
    CRYPTO_ALGORITHM,
    CRYPTO_SECRET_KEY,
    Buffer.from(hash.iv, 'hex')
  )
  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, 'hex')),
    decipher.final(),
  ])

  return decrpyted.toString()
}

export { encrypt, decrypt }

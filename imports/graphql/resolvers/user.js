import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { findUser, insertUser } from '../../database/collections.js'
import { getRandomId } from '../../helpers/getRandomId.js'
import secretsConfig from '../../helpers/secretsConfig.js'

const userResolvers = {
  Query: {
    currentUser: async (parent, args, context, info) => {
      try {
        const { userId } = context
        const user = await findUser({ _id: userId })
        if (!user) {
          return null
        }

        return user
      } catch (error) {
        console.error(error)
        throw error
      }
    },
  },
  Mutation: {
    signup: async (_, { input }) => {
      try {
        const { name, email: userEmail, password } = input

        const email = userEmail.toLowerCase()
        const user = await findUser({ email })
        if (user) {
          throw new Error('Email already in use')
        }

        const hashedPassword = await bcrypt.hash(password, 12)

        const userId = getRandomId()
        const newUser = { _id: userId, name, email, password: hashedPassword }
        await insertUser(newUser)
        const secret = secretsConfig.JWT_SECRET_KEY
        const token = jwt.sign({ userId }, secret, {
          expiresIn: secretsConfig.JWT_EXPIRE,
        })
        return { token, userId }
      } catch (error) {
        console.log(error)
        throw error
      }
    },
    login: async (_, { input }) => {
      try {
        const email = input.email.toLowerCase()
        const user = await findUser({ email })
        if (!user) {
          throw new Error('User not found')
        }

        const isPasswordValid = await bcrypt.compare(input.password, user.password)
        if (!isPasswordValid) {
          throw new Error('Incorrect Password')
        }

        const userId = user._id

        const token = jwt.sign({ userId }, secretsConfig.JWT_SECRET_KEY, {
          expiresIn: secretsConfig.JWT_EXPIRE,
        })
        return { token, userId }
      } catch (error) {
        console.error(error)
        throw error
      }
    },
    editUser: async (parent, args, context, info) => {},
  },
}

export default userResolvers

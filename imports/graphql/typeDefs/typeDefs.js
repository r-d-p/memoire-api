import { gql } from 'apollo-server-express'
import user from './user.js'

const typeDefs = gql`
  scalar Date

  type Query {
    _: String
  }
  type Mutation {
    _: String
  }
`

const typeDefsData = [typeDefs, user]

export default typeDefsData

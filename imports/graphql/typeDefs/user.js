import { gql } from 'apollo-server-express'

const typeDefs = gql`
  extend type Query {
    currentUser: User
  }

  extend type Mutation {
    signup(input: signupInput): SignupData
    login(input: loginInput): LoginData
    editUser(input: editUserInput): User
  }

  input loginInput {
    email: String
    password: String
  }

  input signupInput {
    name: String!
    email: String!
    password: String!
  }

  input editUserInput {
    name: String
    picture: String
  }

  type Token {
    token: String!
  }

  type LoginData {
    token: String
    userId: String
  }

  type SignupData {
    token: String
    userId: String
  }
  type User {
    _id: String
    name: String
    email: String
    createdAt: Date
    updatedAt: Date
  }
`
export default typeDefs

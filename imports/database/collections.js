import MongoDb from 'mongodb'
import secretsConfig from '../helpers/secretsConfig'

const { MongoClient } = MongoDb

let db

const connection = async () => {
  try {
    const url = secretsConfig.MONGO_URL
    const client = new MongoClient(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    await client.connect()
    db = client.db('memories') // mongodb database name
  } catch (error) {
    console.error(error)
    throw error
  }
}

const findOne = async (collectionName, filter) => db.collection(collectionName).findOne(filter)

const findMany = async (collectionName, filter) =>
  db
    .collection(collectionName)
    .find(filter)
    .toArray()

const insertOne = async (collectionName, data) => db.collection(collectionName).insertOne(data)

const findUser = async (filter) => findOne('users', filter)
const findUsers = async (filter) => findMany('users', filter)
const insertUser = async (data) => insertOne('users', data)

export { connection, findUser, findUsers, insertUser }

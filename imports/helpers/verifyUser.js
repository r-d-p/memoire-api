import jwt from 'jsonwebtoken'
import secretsConfig from './secretsConfig'

const verifyUser = async (req) => {
  try {
    const bearerHeader = req?.headers?.authorization
    if (bearerHeader) {
      const token = bearerHeader.split(' ')[1]
      const payload = jwt.verify(token, secretsConfig.JWT_SECRET_KEY)
      const { userId } = payload || {}
      return { userId }
    }

    return { userId: null }
  } catch (error) {
    console.log(error)
    throw error
  }
}

export default verifyUser

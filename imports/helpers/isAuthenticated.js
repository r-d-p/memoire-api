import { skip } from "graphql-resolvers";

const isAuthenticated = (_, __, { _id }) => {
  if (!_id) {
    throw new Error("Access Denied");
  }

  return skip;
};

export default isAuthenticated;
